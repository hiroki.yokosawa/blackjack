import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

/** 標準入力を行うためのクラス. */
class Input{

    private BufferedReader br;

    /** 
     * BufferedReaderのインスタンスを生成する. 
     */
    public Input(){
        br = new BufferedReader(
            new InputStreamReader(System.in));
    }

    /** 
     * 数字入力を行い, 入力された数値を返却する. 
     */
    public int getInputNumber(){
        int number = 0;
        try{
            number = Integer.parseInt(br.readLine());
        }catch(IOException e){
            System.out.println("Exception :" + e);
        }
        return number;
    }
}