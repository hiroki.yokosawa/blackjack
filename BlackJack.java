/**
 * class Deck, class Chips, class Hands, class Inputを駆使して、BlackJackを行うための処理を成立させる.
 */
class BlackJack{

private	Deck deck;
private	Hands ownHands;
private	Hands dealerHands;
private	Chips chips;
private	Input input;
private	int statusKey = -1;
private	boolean isStand	= false;
private	boolean isDealerStand = false;
private static final String[] STATUSCODES = {"win", "lose", "draw", "Black Jack!! win" , "three seven!! win" , "five card!! win"};
private static final int[] ENABLERATE = {10, 50, 100};
private boolean debugMode = false;

    /** 
     * 次のインスタンスを生成し, フィールド変数に代入する.
     * new Deck();
     * new Hands();
     * new Hands(true);
     * new Chips();
     * new Input();
     */
    public BlackJack(){
        this.deck = new Deck();
        this.ownHands = new Hands();
        this.dealerHands = new Hands(true);
        this.chips = new Chips();
        this.input = new Input();
    }

    /**
     * BlackJackを実行する.
     */
    public void execute(){
    	
    	System.out.println("デバッグモードに変更しますか？　0:ON 1:OFF");
    	int debugNumber = input.getInputNumber();
    	if(debugNumber == 0) {
    		this.debugMode = true;
    	}
    	
        this.doBet();
        this.doFirstDeal();
        if(statusKey == -1){
        	this.showAllHand();
        	this.doSelectActionAndJudge();        	
        }
        this.doSettleUp();
        this.doInits();
        if(this.isContinue()){
        	this.execute();
        }
    }

    /**
     * 「いくらBetしますか？ [0] 10枚 [1] 50枚 [2] 100枚」と表示され、Betの処理を行う。betはChipsクラスの値を書き換えることで設定する.
     */
    private void doBet(){
        System.out.println("いくらBetしますか？ [0] 10枚 [1] 50枚 [2] 100枚");
        System.out.println("現在の所持チップ " + this.chips.getChips());

        int inputNumber = 0;
        int betChips = 0;
        try{
            inputNumber = input.getInputNumber();
            if(inputNumber < 0 || inputNumber > 2){
                System.out.println("0,1,2のどれかを入力してください");
                this.doBet();
                return;
            }else {
            	betChips = ENABLERATE[inputNumber];
            	this.chips.setUseChips(betChips);            	
            }
        }catch(Exception e){
            System.out.println("0,1,2のどれかを入力してください");
            this.doBet();
            return;
        }
        
        if(!this.chips.isEnough()){
           this.buyChips();
           return;
        }
        else{
            System.out.println("現在の所持チップ " + this.chips.getChips());

            this.chips.setChips(-this.chips.getUseChips());
            System.out.println("bet後の所持チップ " + this.chips.getChips());
            System.out.println(betChips + "枚Betしました。ゲームスタート");
            return;
        }
    }

    /** 
     * chipの購入を行う。chipは10枚単位での購入が可能.
     * 指定枚数が10枚単位でない場合, 再度入力を求める
     */
    private void buyChips(){
    	
        System.out.println("チップ購入を購入します");
        System.out.println("現在のチップ " + this.chips.getChips());
        // System.out.println(this.chips.getChips());
        int inputNumber = 0;
        try{
            inputNumber = input.getInputNumber();
            if(!(inputNumber % 10 == 0)){
                System.out.println("10円単位でしか購入できません");
                this.buyChips();  
                return;
            }
        }catch(Exception e){
            System.out.println("10円単位でしか購入できません");
            this.buyChips();
            return;
        }
        this.chips.setChips(inputNumber);
        this.doBet();
    }

    /**
     * Deckよりカードを1枚引いて引数として渡されたHandsクラスのaddを利用し, カードをHandに加える.
     */
    private void doDeal(Hands hand){
        int drawCard = deck.drawCard();
        // System.out.println("追加カードは" + drawCard);
        if(this.debugMode) {
        	try {
        		int handNumber = input.getInputNumber();
        		if(handNumber >= 1 && handNumber <= 13) {
        			hand.add(handNumber);        		        			
        		}else
        		{
            		System.out.println("1~13を入力してください");
            		this.doDeal(hand);
            		return;        			
        		}
        	}catch(NumberFormatException e) {
        		System.out.println("数字を入力してください");
        		this.doDeal(hand);
        		return;
        	}
        }else {
        	hand.add(drawCard);        	
        }
        	
        hand.setTotal();
    }

    /**
     * 最初に行うDealの処理,player, dealerの順に交互に1回ずつDealし,それぞれ2回Dealする.
     */
    private void doFirstDeal(){
        for(int i = 1 ; i <= 2 ; i++){
        	if(this.debugMode) {

            	try {
            		int ownHandNumber = input.getInputNumber();
            		if(ownHandNumber >= 1 && ownHandNumber <= 13
            				) {
                		this.ownHands.add(ownHandNumber);
            		}else{
                		System.out.println("1~13を入力してください");
                		this.ownHands.init();
            			this.dealerHands.init();
                		doFirstDeal();
                		return;        			
            		}
            		
            		int dealerHandNumber = input.getInputNumber();
            		if(dealerHandNumber >= 1 && dealerHandNumber <= 13) {
            			this.dealerHands.add(dealerHandNumber);     		        			            			
            		}else {
            			System.out.println("1~13を入力してください");
                		this.ownHands.init();
            			this.dealerHands.init();
                		doFirstDeal();
                		return;
            		}
            	}catch(NumberFormatException e) {
            		System.out.println("数字を入力してください");
            		doFirstDeal();
            		return;
            	}
        	}else {
        		this.ownHands.add(deck.drawCard());        		
        		this.dealerHands.add(deck.drawCard());
        	}
        }
        this.ownHands.setTotal();
        this.dealerHands.setTotal();
        if(this.hasNaturalBJ()){
            this.showResult();
            return;
        }
    }

    /**
     * Dealを呼び出す。引数としてownHands or dealerHandsを渡す.
     */
    private void doHit(Hands hand){
        doDeal(hand);
    }

    /**
     * Dealを呼び出す。useChipを倍にして,chipsを減算する.
     */
    private void doDouble(Hands hand){
    	if(this.chips.getUseChips() > this.chips.getChips()) {
    		System.out.println("Chipが足りません");
    		return;
    	}else {
    		this.chips.setChips(-this.chips.getUseChips());
    		this.chips.setUseChips(this.chips.getUseChips() * 2);
    		doDeal(hand);
    	}
    }

    /**
     * Natural Black Jackが発生しているかチェックする.
     * (自分のみ, ディーラーのみ,その両方)
     * チェックしたのち, Natural Black Jackが発生していれば,
     * statusKeyを1, 2, 3のいずれかにセットする
     * 自分のみ ... 3
     * ディーラーのみ ... 1
     * その両方 ... 2
     * 返り値はstatusKeyが -1ではないかどうか
     */
    private boolean hasNaturalBJ(){
        boolean ownBj =  this.ownHands.isNaturalBJ(this.ownHands.getTotal());
        boolean dealerBj = this.dealerHands.isNaturalBJ(this.dealerHands.getTotal());
        if(ownBj && dealerBj){
            this.statusKey = 2;
        }
        else if(ownBj){
            this.statusKey = 3;
        }else if(dealerBj){
            this.statusKey = 1;
        }
        return !(statusKey == -1);
    }

    /**
     * Bustまたは, Standするまでは, Hit or Double or Stand のいずれかを選択する.
     * 以下大まかなフローである。(エラーの考慮は各自すること)
     * 1. 「どうしますか？[0] Hit [1] Double [2] Stand」と出力される
     * 2. 0 or 1 or 2のいずれかを入力し、次の処理を実行する
     * 　 0 ... doHit()を呼び出し,
     * 　 1 ... Chipsの過不足がないか確認し,足りていれば, doDouble()を呼び出し,
     * 　 2 ... isStandをtrueにする
     * 3. playerがBustになっているか確認し, Bustしていれば, 
     * 　this.statusKeyに1をセットし,関数を終了する。
     * 　表示例は「あなたのBustです。」
     * 4. Dealerの合計が17未満かどうかチェックし,
     * 　17未満であればDealerがdoHit()を実行する
     * 5. Dealerが17以上であれば,this.isDealerStandをtrueにする。
     * 6. DealerがBustになっているか確認し,
     * 　Bustしていればthis.statusKeyに0をセットし,関数を終了する。
     * 　表示例は「DealerがBustです。」
     * 7. this.isStand, this.isDealerStandどちらもtrueの場合は
     * 　player, DealerのHandの合計を比較し,大きい方が勝ち,同じなら引き分けと
     * 　なる様にthis.statusKeyをセットする
     * 8. どちらもtrueでなければ doSelectActionAndJudge()を再度呼び出す。
     * 9. this.isStandがtrueの場合は, 1 ? 3の処理は行わない 
     */
    private void doSelectActionAndJudge(){
        int inputNumber = 0;

        if(!this.isStand){
            if(!this.ownHands.isBust()){
                System.out.println("「どうしますか？[0] Hit [1] Double [2] Stand」");
                try{
                    inputNumber = input.getInputNumber();
                    if(inputNumber < 0 || inputNumber > 2){
                        System.out.println("0,1,2のどれかを入力してください");
                        this.doSelectActionAndJudge();  
                    }
                }catch(Exception e){
                    System.out.println("0,1,2のどれかを入力してください");
                    this.doSelectActionAndJudge();
                }

                switch(inputNumber){
                case 0 :
                    this.doHit(this.ownHands);
                    break;
                case 1 :
                	this.doDouble(this.ownHands);
                    break;
                case 2 :
                    this.isStand = true;
                    break;
                default:
                    System.out.println("例外");
                    break;
                }
            }
	        
        	if(this.ownHands.getTotal() == 21){
        		int sevenCuount = 0;
        		for(int i = 0 ; i < this.ownHands.getHands().size() ; i++){
        			if(this.ownHands.getHands().get(i) == 7) {
        				sevenCuount ++;
        			}
        		}
        		if(sevenCuount == 3) {
        			this.statusKey = 4;
        		}else {
        			this.statusKey = 0;	        		
        		}
        		this.showResult();
        		return;
        	}
        	else if(this.ownHands.isBust()){
        		this.statusKey = 1;
        		this.showResult();
        		System.out.println("あなたのBustです。");
        		return;
        	}
        	
            if(this.ownHands.getHands().size() == 5) {
            	this.statusKey = 5;
                this.showResult();
            	return;
            }	
        }
        
        if(this.dealerHands.getTotal() < 17){
            this.doHit(this.dealerHands);
            if(this.dealerHands.getTotal() == 21){
                this.statusKey = 1;
                this.showResult();
                return;
            }
            
        }
        if(this.dealerHands.isBust()){
            this.showResult();
            System.out.println("DealerのBustです。");
            this.statusKey = 0;
            return;
        }
        else{
            this.isDealerStand = true;
        }

        if(this.isStand) {
            if(this.dealerHands.getTotal() < 17){
                this.doHit(this.dealerHands);
                if(this.dealerHands.getTotal() > 17){
                    this.showResult();
                    return;
                }
            }
            else{
                this.isDealerStand = true;
            }
            if(this.dealerHands.getTotal() == 21){
                this.statusKey = 1;
                this.showResult();
                return;
            }
            else if(this.dealerHands.isBust()){
                this.showResult();
                System.out.println("DealerのBustです。");
                this.statusKey = 0;
                return;
            }
        }
        this.showAllHand();
        
        if(this.isStand == true && this.isDealerStand == true){
            this.showResult();
        }else{
            this.doSelectActionAndJudge();
            return;
        } 
    }

    /** 
     * 次の表示を参考に,全てのHandを出力する.
     * 例1. 結果が出ていない場合)
     * Handの状態
     * あなたのHand: 13 4
     * あなたの合計: 14
     * DealerのHand: ? 10
     * 例2. 結果が出た後)
     * Handの状態
     * あなたのHand: 13 4 13
     * あなたの合計: 24
     * DealerのHand: 13 10
     * Dealerの合計: 20
     */
    private void showAllHand(){
        System.out.println("Handの状態");
        System.out.print("あなたのHand : ");
        ownHands.showHands();
        System.out.print("あなたの合計 : ");
        ownHands.showTotal();
        System.out.print("DealerのHand : ");
        dealerHands.showHands();
        if(!(statusKey == -1)){
            System.out.print("Dealerの合計 : ");
            dealerHands.showTotal();
        }
    }

    /**
     * 「ゲームを続けますか？ [0] 継続 [1] 終了」という表示が出たのち、0, 1の入力値に応じて,0 ... true, 1 ... falseを返す.
     */
    private boolean isContinue(){
    	
    	System.out.println(" 「ゲームを続けますか？ [0] 継続 [1] 終了」");
    	int inputNum = input.getInputNumber();
    	if(!(inputNum == 0 || inputNum == 1)) {
    		System.out.println("0か1を入力してください");
    		return this.isContinue();
    	}
    	
    	if(inputNum == 1) {
    		return false;
    	}
        return true;

    }

    /**
     * dealerHands.reverseIsHide()を呼び出したのち,this.showAllHand()を呼び出す.
     * 結果の文字列(win, lose, draw, Black Jack!!win)を出力する
     */
    private void showResult(){
        dealerHands.reverseIsHide();
        this.showAllHand();
        Integer ownResult = ownHands.getTotal();
        Integer dealerResult = dealerHands.getTotal();
        // int result = 0;
        if(this.statusKey == 3){
            System.out.println(STATUSCODES[this.statusKey]);
            return;
        }else if(this.statusKey == 4) {
        	System.out.println(STATUSCODES[this.statusKey]);
            return;
        }else if(this.statusKey == 5) {
        	System.out.println(STATUSCODES[this.statusKey]);
            return;
        }
        
        
        if(this.dealerHands.isBust()){
            this.statusKey = 0;
        }else if(this.ownHands.isBust()){
            this.statusKey = 1;
        }else{
            if(ownResult.compareTo(dealerResult) == 0){
                this.statusKey = 2;
            }else if(ownResult.compareTo(dealerResult) < 0){
                this.statusKey = 1;
            }else if(ownResult.compareTo(dealerResult) > 0){
                this.statusKey = 0;
            }
        }

        System.out.println(STATUSCODES[this.statusKey]);
    }

    /**
     * ゲームの結果をもとにCHipsに払い戻しを行う.
     * win ... useChips * 2
     * lose ... nothing.
     * draw ... useChips
     * blackjack ... int(useChips * )
     * 払い戻しを行なったのち、
     * 「獲得Chip: 100枚」の様に,払い戻しChipを表示する
     */
    private void doSettleUp(){
        switch(this.statusKey){
        case 0:
            this.chips.setChips(this.chips.getUseChips() * 2);
            System.out.println("現在のチップ " + this.chips.getChips());
            break;
        case 1:
            System.out.println("現在のチップ " + this.chips.getChips());
            break;
        case 2:
        	this.chips.setChips(this.chips.getUseChips() * 1);
            System.out.println("現在のチップ " + this.chips.getChips());
            break;
        case 3:
            this.chips.setChips(this.chips.getUseChips() * 3);
            System.out.println("現在のチップ " + this.chips.getChips());
        	break;
        case 4:
        	this.chips.setChips(this.chips.getUseChips() * 2);
	        System.out.println("現在のチップ " + this.chips.getChips());
	        break;
        case 5:
        	this.chips.setChips(this.chips.getUseChips() * 2);
	        System.out.println("現在のチップ " + this.chips.getChips());
	        break;    
        default:
            break;
        }
    }
    /**
     * 各フィールドの初期化を行う.
     * this.statusKey = -1;
     * this.isStand = false;
     * this.isDealerStand = false;
     * this.ownHands.init();
     * this.dealerHands.init();
     * this.dealerHands.reverseIsHide();
     * this.deck.init();
     */
    private void doInits(){
        this.statusKey = -1;
        this.isStand = false;
        this.isDealerStand = false;
        this.ownHands.init();
        this.dealerHands.init();
        this.dealerHands.reverseIsHide();
        this.deck.init();
        this.debugMode = false;
    }
}
