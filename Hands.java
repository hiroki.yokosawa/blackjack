import java.util.List;
import java.util.ArrayList;

/** 
 * ハンド(手札)を管理するためのクラス.
 * 主に手札の合計点、デッキから引い多カードの足し込み, 手札の取得, Bustになったかどうかの
 * 判断などを行なっている
 */
class Hands{
    private List<Integer> hands = new ArrayList<Integer>(); 
    private int total = 0; 
    private boolean isHide = false;

    /** 
     * this.init()を呼び出す 
     */
    public Hands(){
        this.init();
    }

    /** 
     * this()を呼び出して,isHideをセットする.
     * (ディーラーは一枚目を非公開にするのでisHideをtrueにする)
     */
    public Hands(boolean isHide){
        this.isHide = isHide;
    }
    
    /** 
     * this.handsを空っぽにする.
     */
    public void init(){
        this.getHands().clear();
    }

    /** 
     * this.handsにカードを追加する.
     */
    public void add(int cardNumber){
        this.getHands().add(cardNumber);
    }

    /** 
     * 所持しているカードの内容からカードの合計値をセットする. 
     */
    public void setTotal(){
        this.total = 0;
        int aceCount = 0;
        for(int i = 0 ; i < this.getHands().size() ; i++){
            if(this.getHands().get(i) == 1){
                this.total += 11;
                aceCount++;
            }else if(this.getHands().get(i) >= 11 && this.getHands().get(i) <= 13){
                this.total += 10;
            }else
            {
                this.total += this.getHands().get(i);
            }
        }   
        if(aceCount >= 1){
            this.total -= 10 * (aceCount - 1);
            if(this.isBust()){
                this.total -= 10;
            }
        }
    }

    /** 
     * this.totalを取得する. 
     */
    public int getTotal(){
        return this.total;
    }

    /** 
     * this.isHideを取得する. 
     */
    public boolean getIsHide(){
        return this.isHide;
    }

    /** 
     *this.isHideを反転する. 
     */
    public void reverseIsHide(){
        this.isHide = !this.isHide;
    }

    /** 
     * カードの合計値を出力する. 
     */
    public void showTotal(){
        System.out.println(this.total);
    }

    /** 
     * this.totalが22以上かどうかを返す. 
     */
    public boolean isBust(){
        boolean bool = false;
        if(this.total >= 22){
            bool = true;
        }
        return bool;
    }

    /** 
     * totalが11以上かどうか. 
     */
    public boolean isElevenOrOver(){
        boolean bool = false;
        if(this.total >= 11){
            bool = true;
        }
        return bool;
    }

    /** 
     * 最初のDealでBlackJackになったかどうか 
     */
    public boolean isNaturalBJ(int total){
        boolean bool = false;
        if(total == 21){
            bool = true;
        }
        return bool;
    }

    /** 
     * 所持しているHandを公開する. 
     * (ただし,isHideがtrueの場合は1枚目を非公開にする)
     */
    public void showHands(){
        String str = "";
        // System.out.println(this.getIsHide());
        if(this.isHide){
            for(int i = 0 ; i < this.getHands().size() ; i++){
                if(i == 0){
                    str += "? ";
                }
                else{
                    if(this.getHands().get(i) == 1){
                        str += ("A" + " ");
                    }else if(this.getHands().get(i) == 11 ){
                        str += ("J" + " ");
                    }else if(this.getHands().get(i) == 12 ){
                        str += ("Q" + " ");
                    }else if(this.getHands().get(i) == 13 ){
                        str += ("K" + " ");
                    }else{
                        str += (String.valueOf(this.getHands().get(i)) + " ");
                    }
                }
            }
        }
        else{
            for(int i = 0 ; i < this.getHands().size() ; i++){
                if(this.getHands().get(i) == 1){
                    str += ("A" + " ");
                }else if(this.getHands().get(i) == 11 ){
                    str += ("J" + " ");
                }else if(this.getHands().get(i) == 12 ){
                    str += ("Q" + " ");
                }else if(this.getHands().get(i) == 13 ){
                    str += ("K" + " ");
                }else
                {
                    str += (String.valueOf(this.getHands().get(i)) + " ");
                }
            }

        }

        System.out.println(str);
    }

    public List<Integer> getHands() {
        return hands;
    }

    public void setHands(List<Integer> hands) {
        this.hands = hands;
    }
}