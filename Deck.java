import java.util.ArrayList;
import java.util.Collections;
/** 
 * デッキ(山札)を管理するためのクラス.
 * デッキの初期化, シャッフル, カードの取り出しを行なっている
 */
class Deck{
    private int DECK_COUNT = 6;
    private int ONE_DECK_CARD_COUNT = 52;
    private ArrayList<Integer> defaultDeck = new ArrayList<Integer>();
    private ArrayList<Integer> deck;

    /** 
     * Constructor, defaultDeckにカードをセットする, init()を呼び出す. 
     */
    public Deck(){
        int count = 0;
        for(int i = 1 ; i <= DECK_COUNT ; i++){
            for(int j = 1 ; j <= ONE_DECK_CARD_COUNT ; j++){
                if(j <= 13){
                    defaultDeck.add(j);
                }else if(j >= 14 && j <= 26){
                    defaultDeck.add(j - 13);
                }else if(j >= 27 && j <= 39){
                    defaultDeck.add(j - 26);;
                }else{
                    defaultDeck.add(j - 39);
                }
                count++;
            }
        }
        this.init();
    }

    /** 
     * deckにdefaultDeckをコピーし,shuffleDeck()を呼び出す. 
     */
    public void init(){
        this.deck = this.defaultDeck;
        this.shuffleDeck();
    }

    /** 
     * deckをshuffleする. 
     */
    public void shuffleDeck(){
        Collections.shuffle(this.deck);
    }

    /** 
     * 0番目からカードを削除し, 返却する. 
     */
    public int drawCard(){
        int drawCard = this.deck.get(0);
        this.deck.remove(0);
        return drawCard;
    }
}
