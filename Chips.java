/** 
 * 賭け金の管理, 持っているチップの枚数と, Betしているチップの枚数を保持し、更新・取得できるようにしている.
 */
class Chips{
    private	int chips;
    private	int useChips;

    /** 
     * this.useChips, this.chipsに初期値を設定する.
     */
    public Chips(){
        this.chips = 0;
        this.useChips = 0;
    }

    /** 
     * this.chipsを返却する 
     */
    public int getChips(){
        return this.chips;
    }

    /** 
     * chipsをthis.chipsに代入する. 
     */
    public void setChips(int chips){
        this.chips += chips;
    }

    /** 
     * this.useChipsを返却する. 
     */
    public int getUseChips(){
        return this.useChips;
    }

    /** 
     * this.chipsにint chipsを代入する 
     */
    public void setUseChips(int useChips){
        this.useChips = useChips;
    }

    /** 
     * this.chips >= this.useChipsかどうかを返す. 
     */
    public boolean isEnough(){
        boolean bool = false;
        if(this.useChips == 0){
            bool = false;
        }
        if(this.chips >= this.useChips)
        {
            bool = true;
        }
        return bool;
    }
}
